import math

def vecNormAndMulti(vec, num):
    return vecMulti(vecNorm(vec), num)

def vecNorm(vec):
    length = vecLen(vec)
    if length == 0: return [0, 0]
    return [vec[0] / length, vec[1] / length]

def vecLen(vec):
    return math.hypot(vec[0], vec[1])

# should be acos in front but without is cheaper
def vecAngle(vec1, vec2):
    divider = vecLen(vec1) * vecLen(vec2)
    if divider == 0:
        return 1
    return (vec1[0] * vec2[0] + vec1[1] * vec2[1]) / divider

def printVector(vec):
    print("x:[" + str(vec[0]) + "]y:[" + str(vec[1]) + "]")


sign = lambda x: math.copysign(1, x)
min = lambda x1, x2: x1 if x1 < x2 else x2
max = lambda x1, x2: x1 if x1 > x2 else x2
vecSum = lambda vec1, vec2: [vec1[0] + vec2[0], vec1[1] + vec2[1]]
vecSub = lambda vec1, vec2: [vec1[0] - vec2[0], vec1[1] - vec2[1]]
vecMulti = lambda vec, num: [vec[0] * num, vec[1] * num]
vecScalarMulti = lambda vec1, vec2: vec1[0] * vec2[0] + vec1[1] * vec2[1]

