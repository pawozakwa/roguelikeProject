import sys
import pygame
import random
import copy
from math import *
from pygame.locals import *
from pygame import *

from helpers import VecHelp

DEBUG_BUILD = False  
#DEBUG_BUILD = True

SEED = 16548

screenHeight = 700  # px
screenWidth = 1200 # px
GUI_PART = 0.2
screenLevelWidth = screenWidth * (1.0 - GUI_PART)

WHITE = 255, 255, 255
DARK_GREEN = 0, 50, 0
BLACK = 0, 0, 0
PURPLE = 160, 50, 160
RED = 255, 0, 0
ORANGE = 255, 165, 0
GREEN = 0, 255, 0
YELLOW = 0, 255, 255
BLUE = 100, 150, 255

### GAMEPLAY SETTINGS
IPS = INITIAL_PLAYER_SPEED = 100
INITIAL_PROJECTILE_SPEED = 2
IC = INITIAL_COOLDOWN = 0.3

### LEVEL SETTINGS
CS = CELL_SIZE = 20
MRS = MAX_ROOM_SIZE = 7
DPFTW = DESIRED_PROPORTION_FLOOR_TO_WALL = 0.6
MRL = MAX_RANDOM_LOOPS = 5000
AOP = AMOUNT_OF_PICKUPS = 25
AOD = AMOUNT_OF_DECALS = 70
AOE = AMOUNT_OF_ENEMIES = 7

listOfOpened = []
listOfCorrect = []

class GameObject:
    def __init__(self, startPos):       
       self.position = startPos
       self.target = self.position
       #self.image
       self.color = BLACK

    def Draw(self, window):

        pass

    def Update(self, deltaTime):
        pass

    def getPosOnGrid(self):
        return [int(self.position[0]/CELL_SIZE), int(self.position[1]/CELL_SIZE)]
        
class Player(GameObject):

    def __init__(self, pos, color, game):  
              
        #pos = VecHelp.vecMulti(pos, CELL_SIZE)
        self.size = CS    #srednica
        self.speed = IPS   #predkosc
        self.color = color
        self.target = pos
        self.playerFont = pygame.font.SysFont("monospace", 15)
        self.position = pos
        self.moveDirection = [0, 0]

        self.imageLeft = pygame.image.load("images/nwizardIdle.png")
        self.atckimageLeft = pygame.image.load("images/nwizardAtack.png")
        self.bloodImage = pygame.image.load("images/bloodSplash.png")
        
        self.imageRight = pygame.transform.flip(self.imageLeft, True, False)
        self.atckimageRight = pygame.transform.flip(self.atckimageLeft, True, False)        

        self.lookingRight = True

        self.game = game
        self.projectileInstantiatePosition = VecHelp.vecSum(self.position, [10, 10])

        self.health = 3
        self.fireMana = 1
        self.iceMana = 1
        self.gold = 0

        self.shotCooldown = IC
        self.bloodImageShowTimer = 0.0

    def Update(self, deltaTime, lvl):        
        gridPos = lvl.getGridPosition(self.position)        

        self.getOtherInput(lvl)

        if self.position != self.target:
            targetCell = lvl.getCellAtPos(self.target)
            if not targetCell.isCrossable() or (targetCell.containDoor and not targetCell.doorOpened):
                self.target = self.position            
            dest = VecHelp.vecSub(self.target, self.position)
            disToTarget = VecHelp.vecLen(dest)
            dest = VecHelp.vecMulti(VecHelp.vecNorm(dest), deltaTime * self.speed)
            if disToTarget < VecHelp.vecLen(dest):
                self.position = self.target
                return
            self.position = VecHelp.vecSum(self.position, dest)

            self.projectileInstantiatePosition = VecHelp.vecSum(self.position, [self.imageLeft.get_width()/2 , self.imageLeft.get_height()/2])

            
        else:
            self.getMovementInput()

        if self.shotCooldown > 0:
            self.shotCooldown -= deltaTime

        if self.bloodImageShowTimer > 0:
            self.bloodImageShowTimer -= deltaTime
          
    def getMovementInput(self):
        keys = pygame.key.get_pressed()
        if keys[K_a]:            
            self.target = VecHelp.vecSum(self.target, [-CELL_SIZE, 0])
        if keys[K_d]:
            self.target = VecHelp.vecSum(self.target, [CELL_SIZE, 0])
        if keys[K_s]:
            self.target = VecHelp.vecSum(self.target, [0, CELL_SIZE])
        if keys[K_w]:
            self.target = VecHelp.vecSum(self.target, [0, -CELL_SIZE])   

        self.lookingRight = pygame.mouse.get_pos()[0] < self.position[0]

    def getOtherInput(self, lvl):        
        keys = pygame.key.get_pressed()
        
        if self.shotCooldown < 0:
            if keys[K_e] and self.fireMana > 0:
                self.SendProjectile(pygame.mouse.get_pos(), INITIAL_PROJECTILE_SPEED, Projectile.FIRE)
                self.shotCooldown = IC             
                self.fireMana -= 1
                GUI.GUIinstance.askForRedraw()

            if keys[K_q] and self.iceMana > 0:
                self.SendProjectile(pygame.mouse.get_pos(), INITIAL_PROJECTILE_SPEED, Projectile.ICE) 
                self.shotCooldown = IC   
                self.iceMana -= 1   
                GUI.GUIinstance.askForRedraw()
            
        if keys[K_SPACE]:
            self.doorOpen(lvl, lvl.Cells[int(self.position[0]/CELL_SIZE + 1)][int(self.position[1]/CELL_SIZE + 1)])
            self.doorOpen(lvl, lvl.Cells[int(self.position[0]/CELL_SIZE - 1)][int(self.position[1]/CELL_SIZE - 1)])
            self.doorOpen(lvl, lvl.Cells[int(self.position[0]/CELL_SIZE - 1)][int(self.position[1]/CELL_SIZE + 1)])
            self.doorOpen(lvl, lvl.Cells[int(self.position[0]/CELL_SIZE + 1)][int(self.position[1]/CELL_SIZE - 1)])
            self.doorOpen(lvl, lvl.Cells[int(self.position[0]/CELL_SIZE)][int(self.position[1]/CELL_SIZE + 1)])
            self.doorOpen(lvl, lvl.Cells[int(self.position[0]/CELL_SIZE + 1)][int(self.position[1]/CELL_SIZE)])
            self.doorOpen(lvl, lvl.Cells[int(self.position[0]/CELL_SIZE)][int(self.position[1]/CELL_SIZE - 1)])
            self.doorOpen(lvl, lvl.Cells[int(self.position[0]/CELL_SIZE - 1)][int(self.position[1]/CELL_SIZE)])

    def doorOpen(self, level, cell):
        if cell.containDoor and not cell.doorLocked:
            cell.doorOpened = True
            level.GenerateLevelGraphics()

    def SendProjectile(self, target, speed, kind):
        if self.fireMana > 0 and self.shotCooldown <= 0:
            self.game.projectiles.append(Projectile(self.projectileInstantiatePosition, speed, target, kind))

    def Draw(self, window):
        if self.shotCooldown > 0:
            if self.lookingRight:
                window.blit(self.atckimageLeft, VecHelp.vecSub(self.position, [0, 10]))
            else:
                window.blit(self.atckimageRight, VecHelp.vecSub(self.position, [0, 10]))
        else:
            if pygame.mouse.get_pos()[0] < self.position[0]:
                window.blit(self.imageLeft, VecHelp.vecSub(self.position, [0, 10]))
            else:
                window.blit(self.imageRight, VecHelp.vecSub(self.position, [0, 10]))
        if self.bloodImageShowTimer > 0.0:
            window.blit(self.bloodImage, self.position)
        pass

    def getCellPosition(self):
        return (self.position[0] / CELL_SIZE, self.position[1] / CELL_SIZE)

    def hurtPlayer(self, dmg):
        self.health -= dmg
        self.bloodImageShowTimer = 0.5
        GUI.GUIinstance.askForRedraw()

    def pickupPowerUp(self, type):
        if type == PowerUp.HEALTH_POTION:
            self.health += 1
        elif type == PowerUp.FIRE_POTION:
            self.fireMana += 1
        elif type == PowerUp.ICE_POTION: 
            self.iceMana += 1
        elif type == PowerUp.COINS or type == PowerUp.CHARM or type == PowerUp.BARS:
            self.gold += type
        GUI.GUIinstance.askForRedraw()

class Projectile(GameObject):
    #Kind of projectile
    FIRE = 0
    ICE = 1

    ARROW = 3

    fireImg = None
    iceImg = None

    def __init__(self, pos, speed, tar, kind):
        Projectile.initClass()
        self.position = pos
        self.target = tar
        self.speed = speed
        self.kind = kind
        self.enable = True
        direction = VecHelp.vecSub(self.target, self.position)
        normalDirection = VecHelp.vecNorm(direction)
        self.moveDirection = VecHelp.vecMulti(normalDirection, self.speed)
        if self.kind == Projectile.FIRE:          
            self.image = Projectile.fireImg
        elif self.kind == Projectile.ICE:
            self.image = Projectile.iceImg
        self.imageWidth = self.image.get_rect().width  # should be dynamic
        self.imageHeight = self.image.get_rect().height # should be dynamic too        

        self.drawPosition = VecHelp.vecSub(self.position, [self.imageWidth, self.imageHeight])
        angle = VecHelp.vecAngle([0, -1], normalDirection) #start vector cause of initial direction of img
        degAngle = degrees(angle)
        print("angle: " + str(degAngle))
       
        self.rotatedImage = pygame.transform.rotate(self.image, degAngle)
        if self.moveDirection[0] <= 0:
            self.rotatedImage = pygame.transform.flip(self.rotatedImage, True, False)
            pass

        self.collisionMask = pygame.mask.from_surface(self.rotatedImage, 200)
        self.collisionRect = pygame.Rect(self.position[0] - self.imageWidth / 2, self.position[1] - self.imageHeight / 2, self.position[0] + self.imageWidth / 2, self.position[1] + self.imageHeight / 2)

    def initClass():
        if Projectile.fireImg == None:
            Projectile.fireImg = pygame.image.load("images/fireball.png")
        if Projectile.iceImg == None:
            Projectile.iceImg = pygame.image.load("images/iceball.png")

    def Update(self, deltaTime, game):
        self.checkForCollisions(game)
        if(self.position[0] < 0 or self.position[0] > screenLevelWidth or self.position[1] < 0 or self.position[1] > screenHeight):
            self.enable = False
        if(self.enable):
            self.position = VecHelp.vecSum(self.position, self.moveDirection)
            self.collisionRect = pygame.Rect(self.position[0] - self.imageWidth / 2, self.position[1] - self.imageHeight / 2, self.position[0] + self.imageWidth / 2, self.position[1] + self.imageHeight / 2)
        return super().Update(deltaTime)

    def Draw(self, window):
        if(self.enable):
                
            self.drawPosition = VecHelp.vecSub(self.position, [self.imageWidth / 2, self.imageHeight / 2])
            window.blit(self.rotatedImage, self.drawPosition)
        return super().Draw(window)

    def checkForCollisions(self, game):
        #TO DO checking collisions with walls
        thisRect = pygame.Rect(self.position[0] - self.imageWidth / 4, self.position[1] - self.imageHeight/ 4, self.imageWidth /2, self.imageHeight /1.5)  
        for c in game.level.solidCells:
                if c.Type == Cell.Wall or (c.containDoor and not c.doorOpened):
                    if c.collisionRect.colliderect(thisRect):
                        self.explode()
                        break
        for e in game.enemies:
            
            if e.alive and e.collisionRect.colliderect(thisRect):
                        self.explode()
                        e.health -= 1
                        break
  
    def explode(self):
        self.enable = False
              
class Enemy(GameObject):

    #Elements
    STANDARD = 2
    FIRE = 0
    ICE = 1

    #Kinds of enemies

    def __init__(self,  pos, game):        
        self.alive = True
        self.position = pos
        self.target = pos
        self.moveDirection = [0, 0]
        
        self.imageLeft = pygame.image.load("images/skeleton.png")   
        self.atckimageLeft = pygame.image.load("images/skeleton_attack.png")     
        self.imageDead = pygame.image.load("images/skeletonDead.png")   
        self.imageRight = pygame.transform.flip(self.imageLeft, True, False)
        self.atckimageRight = pygame.transform.flip(self.atckimageLeft, True, False)
        self.lookingRight = True
        self.imageWidth = self.imageLeft.get_rect().width  
        self.imageHeight = self.imageLeft.get_rect().height
        self.collisionRect = self.collisionRect = pygame.Rect(self.position[0], self.position[1], self.imageWidth, self.imageHeight)
        
        self.game = game

        self.health = 2

        self.speed = INITIAL_PLAYER_SPEED

        self.coolDown = 1.0;

        #contain all steps of the 
        self.pathList = []

    def Update(self, deltaTime, game):        
        if self.alive:
            gridPos = game.level.getGridPosition(self.position)    
            self.collisionRect = self.collisionRect = pygame.Rect(self.position[0], self.position[1], self.imageWidth, self.imageHeight)    

            #self.getOtherInput(lvl)
            if self.coolDown > 0.0:
                self.coolDown -= deltaTime

            if self.position == self.target:                
                #DEBUG
                if DEBUG_BUILD:
                    keys = pygame.key.get_pressed()
                    if keys[K_f]:
                        self.pathList = None            
                        self.target = VecHelp.vecSum(self.position, [-CELL_SIZE, 0])
                    if keys[K_h]:
                        self.pathList = None
                        self.target = VecHelp.vecSum(self.position, [CELL_SIZE, 0])
                    if keys[K_g]:
                        self.pathList = None
                        self.target = VecHelp.vecSum(self.position, [0, CELL_SIZE])
                    if keys[K_t]:
                        self.pathList = None
                        self.target = VecHelp.vecSum(self.position, [0, -CELL_SIZE])   
                self.performAI(game)
                if self.pathList:
                    self.target = VecHelp.vecMulti(self.pathList.pop(), CELL_SIZE)
                else:
                    self.performIdleOrWalkAround()
            else:
                if self.target == game.player.position:
                    self.target == self.position
                targetCell = game.level.getCellAtPos(self.target)
                if not targetCell.isCrossable() or (targetCell.containDoor and not targetCell.doorOpened):
                    self.target = self.position            
                dest = VecHelp.vecSub(self.target, self.position)
                
                disToTarget = VecHelp.vecLen(dest)
                dest = VecHelp.vecMulti(VecHelp.vecNorm(dest), deltaTime * self.speed)
                if disToTarget < VecHelp.vecLen(dest):
                    self.position = self.target
                    #print("jest na pozycji")
                    return
                self.position = VecHelp.vecSum(self.position, dest)
            
        if self.health <= 0:
            self.alive = False
            

    def performAI(self, game):
        #print("PERFORM AI")
        if self.checkForPlayerInRange(game):
                if self.coolDown <= 0:
                    game.player.hurtPlayer(1)
                    self.coolDown = 1.0
        elif self.checkForVisionPlayer(game):
            self.pathList = game.pathFinder.getAStarRoute(self.getPosOnGrid(), game.player.getPosOnGrid())
            
            if DEBUG_BUILD:
                print("Sciezka:")
                for step in self.pathList:
                    VecHelp.printVector(step)

            self.pathList.pop()
            pass
        pass

    def checkForVisionPlayer(self, game):
        visionRange = 40.0
        player = game.player
        vectorOfDiffrence = VecHelp.vecSub(player.position, self.position)
        vectorOfDiffrence = [vectorOfDiffrence[0]/CELL_SIZE, vectorOfDiffrence[1]/CELL_SIZE]        
        inRangeX = (vectorOfDiffrence[0] <= visionRange and vectorOfDiffrence[0] >= -visionRange)
        inRangeY = (vectorOfDiffrence[1] <= visionRange and vectorOfDiffrence[1] >= -visionRange)
        return inRangeX and inRangeY

    def checkForPlayerInRange(self, game):
        player = game.player
        vectorOfDiffrence = VecHelp.vecSub(player.position, self.position)
        vectorOfDiffrence = [vectorOfDiffrence[0]/CELL_SIZE, vectorOfDiffrence[1]/CELL_SIZE]        
        inRangeX = (vectorOfDiffrence[0] <= 1.0 and vectorOfDiffrence[0] >= -1.0)
        inRangeY = (vectorOfDiffrence[1] <= 1.0 and vectorOfDiffrence[1] >= -1.0)   
        return inRangeX and inRangeY

    def performIdleOrWalkAround(self):
        idle = random.randint(0, 10)
        if idle == 0:
            dir = random.randint(0, 4)
            if dir == 0:
                self.target = VecHelp.vecSum(self.position, [-CELL_SIZE, 0])        
            if dir == 1:
                self.target = VecHelp.vecSum(self.position, [CELL_SIZE, 0])            
            if dir == 2:   
                self.target = VecHelp.vecSum(self.position, [0, CELL_SIZE])            
            if dir == 3:    
                self.target = VecHelp.vecSum(self.position, [0, -CELL_SIZE])

    def Draw(self, window):
        #print("Rysuje skeletona na: " + string(self.position[0]) + " " + string(self.position[0]))
        if self.alive:
            window.blit(self.imageLeft, VecHelp.vecSub(self.position, [0, 10]))
        else:
            window.blit(self.imageDead, VecHelp.vecSub(self.position, [0, 10]))

        if DEBUG_BUILD:
            pygame.draw.rect(window, BLUE, self.collisionRect, 1)
            pygame.draw.line(window, RED, self.position, self.target)

class PowerUp(GameObject):
    HEALTH_POTION = 0
    FIRE_POTION = 1
    ICE_POTION = 2

    COINS = 10
    CHARM = 30
    BARS = 50

    coinsImage = pygame.image.load("images/coins.png")
    charmImage = pygame.image.load("images/goldCharm.png")
    barImage = pygame.image.load("images/goldBars.png")
    
    healthImage = pygame.image.load("images/potionHealth.png")
    fireImage = pygame.image.load("images/potionFire.png")
    iceImage = pygame.image.load("images/potionIce.png")

    def __init__(self, startPos, type = None):
        self.enable = True
        self.position = startPos
        self.type = type
        if type == None:
            if(random.randint(0, 2) == 0):
                self.type = random.randint(0, 2)
            else:
                los = random.randint(0, 10)
                if los < 2:
                    self.type = PowerUp.BARS
                elif los < 6:
                    self.type = PowerUp.CHARM
                else:
                    self.type = PowerUp.COINS

        self.image = None
        if  self.type == PowerUp.HEALTH_POTION:
            self.image = PowerUp.healthImage
        elif  self.type == PowerUp.FIRE_POTION:
            self.image = PowerUp.fireImage
        elif  self.type == PowerUp.ICE_POTION: 
            self.image = PowerUp.iceImage
        elif  self.type == PowerUp.COINS:
            self.image = PowerUp.coinsImage
        elif  self.type == PowerUp.CHARM:
            self.image = PowerUp.charmImage
        elif  self.type == PowerUp.BARS:
            self.image = PowerUp.barImage        
        return super().__init__(startPos)
      

    def Update(self, deltaTime, game):
        if self.enable and self.getPosOnGrid() == game.player.getPosOnGrid():
            game.player.pickupPowerUp(self.type)
            self.enable = False;

    def Draw(self, window):
        if self.enable:
            window.blit(self.image, self.position)
        
class Decal(GameObject):
    GRASS = 0
    CRACK = 1
    DIRT = 2
    MUD = 3
    WATER = 4

    Images = [[ pygame.image.load("images/decals/grass1.png"), pygame.image.load("images/decals/grass2.png"), pygame.image.load("images/decals/grass2.png")],\
    [ pygame.image.load("images/decals/crack1.png"), pygame.image.load("images/decals/crack2.png")],\
    [ pygame.image.load("images/decals/dirt1.png") ],\
    [ pygame.image.load("images/decals/mud1.png") ],\
    [ pygame.image.load("images/decals/water1.png") ]]

    
    def __init__(self, startPos ):
        self.kind = self.chooseRandomKind()
        self.position = startPos
        self.image = random.choice(self.Images[self.kind])

        return super().__init__(startPos)

    def chooseRandomKind(self):
        r = random.randint(0, 100)
        if r < 50:
            return Decal.GRASS
        if r >= 50 and r < 65:
            return Decal.CRACK
        if r >= 65 and r < 75:
            return Decal.DIRT
        if r >= 75 and r < 80:
            return Decal.MUD
        if r >= 80:
            return Decal.WATER

    def Draw(self, window):
        window.blit(self.image, self.position)
        
class Cell(GameObject):
    #States of level cells
    Empty = 0
    Wall = 1

    def __init__(self, type, x, y):
        #Position of cell is determined as position in structure and in
        #addition stored in object
        self.position = [x, y]
        self.Type = type
        self.ImageVariant = random.randint(0,3)
        self.pixelPosition = [x * CELL_SIZE, y * CELL_SIZE]
        self.collisionRect = pygame.Rect(self.pixelPosition[0] , self.pixelPosition[1], CELL_SIZE, CELL_SIZE)  
        self.containDoor = False
        self.doorLocked = False
        self.doorOpened = False
        self.doorHidden = False
        
        if self.Type == Cell.Wall:
            #print(str(self.collisionRect))
            pass

    def getPosition(self):
        return self.position

    def isCrossable(self):
        if self.Type == Cell.Empty or (self.containDoor and self.doorOpened):
            return True
        else:
            return False

class Level(GameObject):
    def __init__(self, window, cellSize):
        self.levelGenObject = LevelGenerator(SEED)
        self.Width = screenWidth * (1.0 - GUI_PART) / cellSize
        self.Height = screenHeight / cellSize

        
        self.Cells = self.levelGenObject.generateDungeon(self.Width, self.Height)  
        #self.Cells = self.levelGenObject.generateEmptySpace(self.Width, self.Height) 

        self.solidCells = self.generateListOfSolidWalls()
        self.cellSize = cellSize
        self.FloorImages = [pygame.image.load("images/floorTile0.png"), pygame.image.load("images/floorTile1.png"), pygame.image.load("images/floorTile2.png"), pygame.image.load("images/floorTile3.png")]
        self.WallImages = [pygame.image.load("images/wallTile0.png"), pygame.image.load("images/wallTile1.png"), pygame.image.load("images/wallTile2.png"), pygame.image.load("images/wallTile3.png")]
        self.DoorOpenedImage = pygame.image.load("images/openedDoor.png")
        self.DoorClosedImage = pygame.image.load("images/closedDoor.png")        
        
        self.LevelImage = None
        self.GenerateLevelGraphics()

    def GenerateLevelGraphics(self):
        w = int(screenWidth * (1.0 - GUI_PART))
        h = int(screenHeight)
        self.LevelImage = pygame.Surface((w, h))
        self.draw(self.LevelImage)

    def getGridPosition(self, pos):
        gridPos = (pos[0] - pos[0] % self.cellSize, pos[1] - pos[1] % self.cellSize)
        return gridPos

    def getCellAtPos(self, pos):
        return self.Cells[int(pos[0] / self.cellSize)][int(pos[1] / self.cellSize)]
        pass

    def generateListOfSolidWalls(self):
        outPutList = [] 
        for i in range(0, len(self.Cells)):
            for j in range(0, len(self.Cells[i])):
                c = self.Cells[i][j]
                if c.Type == Cell.Wall:
                    #print(str(c))
                    outPutList.append(c)
        return outPutList
                        
    def draw(self, window):
        for i in range(0, len(self.Cells)):
                for j in range(0, len(self.Cells[i])):
                    c = self.Cells[i][j]
                    pos = [i * self.cellSize, j * self.cellSize]
                    
                    if c.Type == Cell.Wall:
                        window.blit(self.WallImages[c.ImageVariant], pos)
                        
                    elif c.Type == Cell.Empty:
                        window.blit(self.FloorImages[c.ImageVariant], pos)

                    if c.containDoor:
                        if not c.doorHidden:
                            if c.doorOpened:
                                window.blit(self.DoorOpenedImage, pos)
                            else:
                                window.blit(self.DoorClosedImage, pos)

                        #there should be doors graphics
                        pass

                    if DEBUG_BUILD:
                        GUI.drawFont(window, pos, str(i), 15, GREEN)
                        GUI.drawFont(window, VecHelp.vecSum(pos, [0,9]), str(j), 15, GREEN)
                        
    def drawBuffered(self, window):
        window.blit(self.LevelImage, [0, 0])

class Room:
    def __init__(self, rect, parent = None):
        self.childs = []
        self.depthOfBranch = 0
        if parent != None:
            parent.childs.append(self)
            self.depthOfBranch = parent.depthOfBranch + 1
        self.rect = rect
        self.doors = []

    def addDoor(self, door):
        self.door.append(door)
    
    def getRandomConnectedRoom(self, door):
        r = random.randint(0, 3)
        doorPoint = [0,0]
        newRoomSize = [random.randint(3, 10), random.randint(3, 10)]
        offsetX = random.randint(1, newRoomSize[0] - 1)
        offsetY = random.randint(1, newRoomSize[1] - 1)
        newRect = None

        if r == 0: # left wall
            #print("lewa strona")
            doorPoint = [self.rect.left - 1, random.randint(self.rect.top, self.rect.bottom - 1)]
            newRect = pygame.Rect(doorPoint[0] - newRoomSize[0], doorPoint[1] - offsetY, newRoomSize[0], newRoomSize[1])

        if r == 1: # right wall
            #print("prawa strona")
            doorPoint = [self.rect.right, random.randint(self.rect.top, self.rect.bottom - 1)]
            newRect = pygame.Rect(doorPoint[0] + 1, doorPoint[1] - offsetY, newRoomSize[0], newRoomSize[1])

        if r == 2: # up wall
            #print("górna strona")
            doorPoint = [random.randint(self.rect.left, self.rect.right - 1), self.rect.top - 1]
            newRect = pygame.Rect(doorPoint[0] - offsetX, doorPoint[1] - newRoomSize[1], newRoomSize[0], newRoomSize[1])

        if r == 3: # bottom wall
            #print("dolna strona")
            doorPoint = [random.randint(self.rect.left, self.rect.right - 1), self.rect.bottom]
            newRect = pygame.Rect(doorPoint[0] - offsetX, doorPoint[1] + 1, newRoomSize[0], newRoomSize[1])

        #print("Wygenerowany pokoj: " + str(newRect))
        #print("door localisation: " + str(doorPoint))
        #doorList.append(doorPoint)
        #door = doorPoint
        return newRect, doorPoint
                        
class LevelGenerator:
    def __init__(self, seed = None):
        self.Cells = []
        self.width = 0
        self.height = 0
        self.seed = seed
        if(seed == None):
            self.seed = time.get_ticks()
        random.seed(self.seed)

        self.rooms = []

    def generateEmptySpace(self, width, height):
        #print("Zaczynam tworzyć poziom")
        self.Cells = []
        self.width = width
        self.height = height
        self.cellsCount = width * height
        self.emptyCount = 0

        #self.placedRoomRects = []

        #Fill 
        for i in range(int(width)):
            self.Cells.append([Cell(Cell.Wall, i, j) for j in range(int(height))]) 
        #Tworzymy przestrzeń w środku
        self.fillEmpty()
        return self.Cells
    
    def generateDungeon(self, width, height):
        print("Zaczynam tworzyć poziom o rozmiarze: " + str(width) + "  " + str(height))
        self.Cells = []
        self.width = width
        self.height = height
        self.cellsCount = width * height
        self.emptyCount = 0

        self.doorsList = []

        #Wypelniamy poziom murem
        for i in range(int(width)):
            self.Cells.append([Cell(Cell.Wall, i, j) for j in range(int(height))])     
               
        #placing starting room
        xStart = random.randint(15, int(self.width / 2 - 2))
        yStart = random.randint(15, int(self.height / 2 - 2))
        xSize = ((self.width/2) - xStart) * 2
        ySize = ((self.height/2) - yStart) * 2
        
        roomRect = pygame.Rect(xStart, yStart, xSize, ySize)
        self.addRoom(roomRect)
        
        #Try to place suitable room connected with randomized doors
        iterations = 0
        rectOfLevel = pygame.Rect(0, 0, width, height)
        while self.emptyCount / self.cellsCount < DESIRED_PROPORTION_FLOOR_TO_WALL and iterations < MAX_RANDOM_LOOPS:
            self.addNewRandomRoom(rectOfLevel)
            iterations += 1

        return self.Cells

    def fillEmpty(self):
        self.addRoom(Rect(1, 1, self.width - 2, self.height - 2))
        return self.Cells

    def addNewRandomRoom(self, rectOfLevel):
        room = random.choice(self.rooms)
        door = None
        newRoomRect, door = room.getRandomConnectedRoom(door)
        wrongPlaceOfRoom = False
        if newRoomRect.right >= rectOfLevel.right or newRoomRect.bottom >= rectOfLevel.bottom or newRoomRect.x <= 0 or newRoomRect.y <= 0:     
                wrongPlaceOfRoom = True             
                return
        for otherRoom in self.rooms:                
            if newRoomRect != otherRoom.rect:
                if self.checkOverlaping(otherRoom.rect, newRoomRect):                        
                        wrongPlaceOfRoom = True
                        newRoomRect = None
                        door = None
                        break                        
        if not wrongPlaceOfRoom:
            self.addRoom(newRoomRect, room)
            if door != None: # if there is no door
                self.doorsList.append(door)
                #print("Umieszczam drzwi: " + str(door))
                self.setDoor(door)
                #DEBUG
                if Rect(door[0], door[1], 0, 0).colliderect(newRoomRect):
                    #print("Drzwi: " + str(door) + " sa wewnatrz")
                    pass

    def addRoom(self, rect, parent = None):
        #if #check do Room is inside level
        for i in range(rect.left, int(rect.right)):
            for j in range(rect.top, int(rect.bottom)):
                #print("Setting cell: " + str(i) + " " + str(j))
                self.setCell([i, j], Cell.Empty)
                #self.Cells[i][j].Type = Cell.Empty
                #self.emptyCount += 1
        #print("Adding room: " + str(rect))
        if parent != None: 
            #print(" with parent: " + str(parent.rect))
            pass
        self.rooms.append(Room(rect, parent))
        return self.Cells

    def getRandomRoom(self):
        return self.rooms[random.randint(0, self.rooms.count - 1)]
        pass

    def setCell(self, pos, kind):
        if pos[0] > self.width or pos[1] > self.height:
            print("cell out of space")
            return

        cell = self.Cells[pos[0]][pos[1]]
        if cell.Type != kind:
            if kind == Cell.Wall:
                cell.Type = Cell.Wall
                self.emptyCount -= 1
            if kind == Cell.Empty:
                cell.Type = Cell.Empty
                self.emptyCount += 1
    
    def setDoor(self, pos):
        cell = self.Cells[pos[0]][pos[1]]
        cell.Type = Cell.Empty
        cell.containDoor = True
        cell.doorOpened = False

    def checkOverlaping(self, room1, room2):
        troom1 = copy.copy(room1)
        troom2 = room2
        troom1.inflate_ip(2, 2)
        return troom1.colliderect(troom2)

    def fillLevelWithPickUps(self):
        pickUps = []
        i = 0
        while i < AMOUNT_OF_PICKUPS:
            room = random.choice(self.rooms)
            result = random.randint(0, 10)
            if result < room.depthOfBranch:
                x = random.randint(room.rect.left, room.rect.right - 1)
                y = random.randint(room.rect.top, room.rect.bottom - 1)
                pickUps.append(PowerUp([x * CELL_SIZE,y * CELL_SIZE]))
                i += 1
        return pickUps

    def fillLevelWithDecals(self):
        decals = []
        for i in range(0, AMOUNT_OF_DECALS):
            room = random.choice(self.rooms)
            x = random.randint(room.rect.left, room.rect.right - 1)
            y = random.randint(room.rect.top, room.rect.bottom - 1)
            decals.append(Decal([x * CELL_SIZE,y * CELL_SIZE]))
        return decals

    def fillLevelWithEnemies(self, game):
        enemies = []
        i = 0
        while i < AMOUNT_OF_ENEMIES:
            room = random.choice(self.rooms)
            result = random.randint(0, 10)
            if result < room.depthOfBranch:
                x = random.randint(room.rect.left, room.rect.right - 1)
                y = random.randint(room.rect.top, room.rect.bottom - 1)
                enemies.append(Enemy([x * CELL_SIZE,y * CELL_SIZE], game))
                i += 1
        return enemies

class PathFinder:

    def __init__(self, level):
        self.level = level
        self.horizontalSize = level.Width
        self.verticalSize = level.Height
        self.cellsColliding = level.solidCells

        """for i in range(self.horizontalSize):
            self.cellsColliding.append([])
            for j in range(self.verticalSize):
                cellColliding = False
                for k in range(len(walls)):
                    if isinstance(walls[k], entities.PolygonWall.PolygonWall):
                        cellColliding = collisionHelper.checkCircleCollisionWithPolygon((self.dummy.radius + i*self.dummy.radius*2, self.dummy.radius + j*self.dummy.radius*2), self.dummy.radius, walls[k].points, walls[k].edges)
                    else:
                        cellColliding = collisionHelper.checkCollision((self.dummy.radius + i*self.dummy.radius*2, self.dummy.radius + j*self.dummy.radius*2), self.dummy.radius, walls[k].position, walls[k].radius)
                    if cellColliding:
                        break
                self.cellsColliding[i].append(cellColliding)"""

    def getAStarRoute(self, start, destination):
        listOfCorrect = []
        listOfOpened = []

        G = [] # shortest distance from start
        H = [] # H distance to end
        previous = [] # double dimension array of previous steps
        dummyNumber = 9999999
        dummyVertex = [-1,-1]
        start = [int(start[0]), int(start[1])]
        for i in range(int(self.horizontalSize)):
            G.append([])
            H.append([])
            previous.append([])
            for j in range(int(self.verticalSize)):
                G[i].append(dummyNumber)
                H[i].append(dummyNumber)
                previous[i].append(Cell(Cell.Wall,-1, -1))
        openList = []
        closedList = []
        #print("x: " + str(start[0]) + "y: " + str(start[1]))
        G[start[0]][start[1]] = 0
        openList.append(start)
        while openList:
            u = self.getLowestFVertex(openList, G, H)

            #draw debug
            listOfOpened.append(u)

            if u == destination:
                return self.generateAStarFoundPathFrom(previous, start, destination, [])
    
            openList.remove(u)
    
            for neighbor in self.getNeighbors(u):
                if (neighbor in openList or neighbor in closedList) and G[neighbor[0]][neighbor[1]]<=G[u[0]][u[1]]+1:
                    continue
                previous[neighbor[0]][neighbor[1]]=u
                G[neighbor[0]][neighbor[1]]=G[u[0]][u[1]]+1
                H[neighbor[0]][neighbor[1]]=self.getEuklidesLength(neighbor, destination)

                #Bigger influence of heuristic
                H[neighbor[0]][neighbor[1]] *= 2
    
                if neighbor in closedList:
                    closedList.remove(neighbor)
                if neighbor not in openList:
                    openList.append(neighbor)
    
            closedList.append(u)
        return []

    def getLowestFVertex(self, list, G, H):
        #return most probably step on shortest path
        # F is sum of G and H distance
        lowestF = 999999999
        lowestFVertex = None
        for vertex in list:
            F=G[vertex[0]][vertex[1]] + H[vertex[0]][vertex[1]]
            if lowestF>F:
                lowestF=F
                lowestFVertex=vertex

        return lowestFVertex

    def generateAStarFoundPathFrom(self, previous, start, destination, path):
        path.append(destination)
        if destination == start:
            return path
        destination = previous[destination[0]][destination[1]]
        listOfCorrect.append(destination)
        return self.generateAStarFoundPathFrom(previous, start, destination, path)

    def getNeighbors(self, vertex):
        neighbors = []
        if vertex[0]>self.horizontalSize-1 or vertex[1]>self.verticalSize-1:
            return neighbors
        if vertex[0]<self.horizontalSize-1 and not self.level.Cells[vertex[0]+1][vertex[1]].Type == Cell.Wall:
            neighbors.append([vertex[0]+1,vertex[1]])
        if vertex[0]>0 and not self.level.Cells[vertex[0]-1][vertex[1]].Type == Cell.Wall:
            neighbors.append([vertex[0]-1,vertex[1]])
        if vertex[1]<self.verticalSize-1 and not self.level.Cells[vertex[0]][vertex[1]+1].Type == Cell.Wall:
            neighbors.append([vertex[0],vertex[1]+1])
        if vertex[1]>0 and not self.level.Cells[vertex[0]][vertex[1]-1].Type == Cell.Wall:
            neighbors.append([vertex[0],vertex[1]-1])
        return neighbors

    def getEuklidesLength(self, start, end):
        return VecHelp.vecLen(VecHelp.vecSub(end,start))

    def testVertexDebugDraw(self, screen, vertexList, color):
        r =  int(CELL_SIZE / 2)
        for v in vertexList:
            pygame.draw.circle(screen, color, [v[0] + r, v[1] + r], r, 1)

class GUI:
    GUIinstance = None
    def __init__(self):
        self.guiHook = [screenLevelWidth, 0]
        self.background = pygame.image.load("images/GUI/gui_back.png")
        self.manaIceIco = pygame.image.load("images/GUI/mana_ice.png")
        self.manaFireIco = pygame.image.load("images/GUI/mana_fire.png")
        self.healthIco = pygame.image.load("images/GUI/health.png")
        self.goldIco = pygame.image.load("images/GUI/gold.png")
        self.needToBeRedraw = True
        GUI.GUIinstance = self
        
    def Draw(self, window, player):
        if self.needToBeRedraw:
            window.blit(self.background, (screenLevelWidth, 0))
            window.blit(self.healthIco, VecHelp.vecSum(self.guiHook, [50, 50]))
            GUI.drawFont(window, VecHelp.vecSum(self.guiHook, [75, 75]), str(player.health), 40, RED)
            window.blit(self.manaFireIco, VecHelp.vecSum(self.guiHook, [50, 125]))
            GUI.drawFont(window, VecHelp.vecSum(self.guiHook, [90, 150]), str(player.fireMana), 40, ORANGE)
            window.blit(self.manaIceIco, VecHelp.vecSum(self.guiHook, [50, 200]))
            GUI.drawFont(window, VecHelp.vecSum(self.guiHook, [90, 225]), str(player.iceMana), 40, BLUE)
            window.blit(self.goldIco, VecHelp.vecSum(self.guiHook, [50, 275]))
            GUI.drawFont(window, VecHelp.vecSum(self.guiHook, [90, 300]), str(player.gold), 40, BLUE)
            self.needToBeRedraw = False

    def askForRedraw(self):
        self.needToBeRedraw = True

    @staticmethod
    def drawFont(window, pos, text, size, color):
        # initialize font; must be called after 'pygame.init()' to avoid 'Font not Initialized' error
        myfont = pygame.font.SysFont("Aldhabi", size)
        # render text
        label = myfont.render(text, 1, color)        
        window.blit(label, pos)

class Game:
    def __init__(self):
        pygame.init()
        pygame.mixer.init()
        pygame.display.set_caption('Gold and dungeons')
        self.gameRunning = True
        self.window = pygame.display.set_mode((screenWidth, screenHeight), 0, 24)
        self.GUIobject = GUI()
        self.level = Level(self.window, CELL_SIZE)
        playerPos = [int(self.level.Width*CELL_SIZE / 2 - (self.level.Width*CELL_SIZE / 2 % CELL_SIZE)), int(self.level.Height*CELL_SIZE/2 - (self.level.Height*CELL_SIZE/2  % CELL_SIZE))]
        self.player = Player(playerPos, BLACK, self)
        self.powerUps = self.level.levelGenObject.fillLevelWithPickUps()
        self.decals = self.level.levelGenObject.fillLevelWithDecals()
        self.enemies = self.level.levelGenObject.fillLevelWithEnemies(self)
        self.projectiles = []


        self.pathFinder = PathFinder(self.level)
            
    def runTheGame(self):
        #while(True):
        print("Odpalam gre")
        game_logo = pygame.image.load("logo.png")
        self.window.fill(BLACK)
        self.window.blit(game_logo, (screenWidth / 2 - 320, screenHeight / 2 - 200))
        pygame.display.update()

        started = False
        # Main menu loop
        while(started == False):
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    started = True

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()
        
        newTime = time.get_ticks()
        
        # Main game loop
        while self.gameRunning:
            ############ CALCULATING TIME STEP
            oldTime = newTime
            newTime = time.get_ticks() 
            dT = newTime - oldTime # dT time in miliseconds

            deltaTime = dT / 1000 # deltaTime time in seconds

            ############ CHECKING DO PLAYER IS ALIVE
            if self.player.health <= 0:

                self.gameRunning = False

            ###DEBUG METHODS
            if DEBUG_BUILD: self.debugUpdate()

            ############ GETTING INPUT
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()                

            keys_pressed = pygame.key.get_pressed()

            if(keys_pressed[K_ESCAPE]):
                pygame.quit()
                sys.exit()

            if keys_pressed[K_LEFT]:
                pass
            elif keys_pressed[K_RIGHT]:
                pass

            ############ UPDATE OF STATES
            self.player.Update(deltaTime, self.level)

            for enemy in self.enemies:
                enemy.Update(deltaTime, self)
            
            for projectile in self.projectiles:
                projectile.Update(deltaTime, self)

            for powerUp in self.powerUps:
                powerUp.Update(deltaTime, self)
            
            

            ############ DELETING DISABLED OBJECTS

            self.cleanDisabledObjects()

            ############ RYSOWANIE
           
            #self.level.draw(self.window)    #Draw whole level at once instead                      
            self.level.drawBuffered(self.window)

            for decal in self.decals:
                decal.Draw(self.window)

            for powerUp in self.powerUps:
                powerUp.Draw(self.window)

            for projectile in self.projectiles:
                projectile.Draw(self.window)

            self.player.Draw(self.window)

            for enemy in self.enemies:
                enemy.Draw(self.window)

            self.GUIobject.Draw(self.window, self.player)

            #draw frame amount
            if deltaTime != 0:
                GUI.drawFont(self.window, (self.window.get_width() - 300, 20), str(int(1.0/deltaTime)), 20, GREEN)

            pygame.display.update()

            ## DEBUG DRAW ##
            if DEBUG_BUILD and pygame.key.get_pressed()[K_p]:
                route = self.pathFinder.getAStarRoute(self.player.getCellPosition(), self.level.getCellAtPos(pygame.mouse.get_pos()).getPosition())
                if route != None:
                    self.pathFinder.testVertexDebugDraw(self.window, route, RED)

            for v in listOfOpened:
                x = [int(v[0] + CELL_SIZE / 2), int(v[1] + CELL_SIZE / 2)]
                print("Wierzcholek: " + x)
                pygame.draw.circle(self.window, RED, x, int(CELL_SIZE/2))
            
            for v in listOfCorrect:
                x = [int(v[0] + CELL_SIZE / 2), int(v[1] + CELL_SIZE / 2)]
                pygame.draw.circle(self.window, GREEN, x, int(CELL_SIZE/2))

        self.window.fill(BLACK)
        GUI.drawFont(self.window, (self.window.get_width()/ 3, self.window.get_height()/ 2), str("GAME OVER!"), 80, RED)
        pygame.display.update()
        
        while(True):
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    break

                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

    def cleanDisabledObjects(self):
        for projectile in self.projectiles:
            if not projectile.enable:
                self.projectiles.remove(projectile)

    def debugUpdate(self):
        if pygame.key.get_pressed()[K_F1]:
            self.player.target = self.player.position = pygame.mouse.get_pos()

G = Game()

G.runTheGame()

pygame.quit()
sys.exit()
